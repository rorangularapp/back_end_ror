# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180309175803) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "aircrafts", force: :cascade do |t|
    t.string "brand"
    t.string "model"
    t.string "plate"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "flight_attributes", force: :cascade do |t|
    t.date "flight_day"
    t.time "start_time"
    t.time "end_time"
    t.string "type_of_flight"
    t.bigint "pilot_id"
    t.bigint "aircraft_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["aircraft_id"], name: "index_flight_attributes_on_aircraft_id"
    t.index ["pilot_id"], name: "index_flight_attributes_on_pilot_id"
  end

  create_table "pilots", force: :cascade do |t|
    t.string "name"
    t.string "last_name"
    t.string "number_of_licence"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "flight_attributes", "aircrafts"
  add_foreign_key "flight_attributes", "pilots"
end
