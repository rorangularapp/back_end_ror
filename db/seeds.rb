# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
pilot = Pilot.create!({
    name: "Alex",
    last_name: "Quintana",
    number_of_licence: "15464"
})

aircraftvar = Aircraft.create!({
    brand: "DMZ",
    model: "Hover",
    plate: "5454654",
    description: "Is a new dron with gun and explosives"
})

FlightAttribute.create!(   
{
    flight_day: Date.today,
    start_time: Time.now,
    end_time: Time.now,
    type_of_flight: "Search enemies",
    pilot: pilot,
    aircraft: aircraftvar
})