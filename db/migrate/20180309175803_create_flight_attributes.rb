class CreateFlightAttributes < ActiveRecord::Migration[5.1]
  def change
    create_table :flight_attributes do |t|
      t.date :flight_day
      t.time :start_time
      t.time :end_time
      t.string :type_of_flight
      t.references :pilot, foreign_key: true
      t.references :aircraft, foreign_key: true

      t.timestamps
    end
  end
end
