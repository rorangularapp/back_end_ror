class CreatePilots < ActiveRecord::Migration[5.1]
  def change
    create_table :pilots do |t|
      t.string :name
      t.string :last_name
      t.string :number_of_licence

      t.timestamps
    end
  end
end
