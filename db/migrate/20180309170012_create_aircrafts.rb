class CreateAircrafts < ActiveRecord::Migration[5.1]
  def change
    create_table :aircrafts do |t|
      t.string :brand
      t.string :model
      t.string :plate
      t.string :description

      t.timestamps
    end
  end
end
