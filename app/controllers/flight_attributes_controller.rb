class FlightAttributesController < ApplicationController
  before_action :set_flight_attribute, only: [:show, :update, :destroy]

  # GET /flight_attributes
  def index
    @flight_attributes = FlightAttribute.all

    render json: @flight_attributes
  end

  # GET /flight_attributes/1
  def show
    render json: @flight_attribute
  end

  # POST /flight_attributes
  def create
    @flight_attribute = FlightAttribute.new(flight_attribute_params)

    if @flight_attribute.save
      render json: @flight_attribute, status: :created, location: @flight_attribute
    else
      render json: @flight_attribute.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /flight_attributes/1
  def update
    if @flight_attribute.update(flight_attribute_params)
      render json: @flight_attribute
    else
      render json: @flight_attribute.errors, status: :unprocessable_entity
    end
  end

  # DELETE /flight_attributes/1
  def destroy
    @flight_attribute.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_flight_attribute
      @flight_attribute = FlightAttribute.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def flight_attribute_params
      params.require(:flight_attribute).permit(:flight_day, :start_time, :end_time, :type_of_flight, :pilot_id, :aircraft_id)
    end
end
