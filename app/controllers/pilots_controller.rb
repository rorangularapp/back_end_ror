class PilotsController < ApplicationController
  before_action :set_pilot, only: [:show, :update, :destroy]

  # GET /pilots
  def index
    @pilots = Pilot.all

    render json: @pilots
  end

  # GET /pilots/1
  def show
    render json: @pilot
  end

  # POST /pilots
  def create
    @pilot = Pilot.new(pilot_params)

    if @pilot.save
      render json: @pilot, status: :created, location: @pilot
    else
      render json: @pilot.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /pilots/1
  def update
    if @pilot.update(pilot_params)
      render json: @pilot
    else
      render json: @pilot.errors, status: :unprocessable_entity
    end
  end

  # DELETE /pilots/1
  def destroy
    @pilot.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pilot
      @pilot = Pilot.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def pilot_params
      params.require(:pilot).permit(:name, :last_name, :number_of_licence)
    end
end
