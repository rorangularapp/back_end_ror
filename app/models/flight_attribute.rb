class FlightAttribute < ApplicationRecord
  belongs_to :pilot
  belongs_to :aircraft
end
