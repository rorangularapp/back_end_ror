Rails.application.routes.draw do
  resources :flight_attributes
  resources :aircrafts
  resources :pilots
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
